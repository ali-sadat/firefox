## Firefox docker image with Xpra

Firefox docker image using Xpra for displaying GUI from container on the host.

## Tested Environment:

```
Debian 9 Desktop Edition
Debian 10 Desktop Edition
```

## Making the Required Change in Dockerfile

Open up Dockerfile and change the below lines:

```
"--bind-tcp=0.0.0.0:10000"
```
to

```
"--bind-tcp=<Your Host IP>:10000"
```

## Building the Docker Image


```
docker build -t iprimo/firefox .
```

## Usage example

Run the container in the background

```
docker run -d --net=host iprimo/firefox
```

# Find out on which local port was the port 10000 from the container exposed

```
docker ps -l
```

## Using Xpra on the host to attach to container's GUI

```
xpra attach tcp:localhost:PORT
```

(replace PORT with the port number exposed from the container)


[Watch Video](https://asciinema.org/a/Y52XjQ0E3ddOUN4iwAJyWe9aO)

## Troubleshooting

In case you face an error related to **libcanberra-gtk-module:i386**, then you might need to install the below package:

```
apt-get install libcanberra-gtk-module

```

